package com.retail.core;

public class Item {
	private String upc;
	private String description;
	private Double price;
	private Double weight;
	private ShipMethod ship;
	// Declaring an enumeration..
	public enum ShipMethod {
	 GROUND	, AIR
	};
	
	public String getUpc() {
		return upc;
	}

	public void setUpc(String upc) {
		this.upc = upc;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}
	public ShipMethod getShip() {
		return ship;
	}

	public void setShip(ShipMethod ship) {
		this.ship = ship;
	}

	//This is a perimeterized constructor
	public Item(String upc,String description,Double price, Double weight,ShipMethod ship) {
		this.upc = upc;
		this.description = description;
		this.price = price;
		this.weight = weight;
		this.ship = ship;
		
	}
	public Item() {}
}

package com.retail.display;

import com.retail.core.Item;


public interface Retail {

	public double getShippingCost(Item item);
}

package com.retail.display;

import com.retail.core.Item;

// Creating a Context class in Strategy pattern whose behavior varies as per its strategy object..
public class ShipContext {

	private Retail retail;

	public ShipContext(Retail retail) {
		this.retail = retail;
	}

	public double executeShippingCost(Item item) {
		return retail.getShippingCost(item);

	}
}

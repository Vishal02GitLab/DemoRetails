package com.retail.display;

import java.math.BigInteger;
import java.util.Comparator;

import com.retail.core.Item;

public class SortUpc implements Comparator<Item> {

	public int compare(Item o1, Item o2) {
		
		BigInteger b1= new BigInteger(o1.getUpc());
		BigInteger b2= new BigInteger(o2.getUpc());
		
		return b1.compareTo(b2);
	}

	
}

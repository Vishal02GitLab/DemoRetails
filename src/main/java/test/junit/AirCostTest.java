package test.junit;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.retail.core.Item;
import com.retail.display.DisplayAirCost;

public class AirCostTest {

	DisplayAirCost air;
	Item item;
	
	@Before
	public void doSetUp() {
		air = new DisplayAirCost();
		item = new Item();
		System.out.println("Opening of TestSuite====== Before");
	}
	
	@Test
	public void testAirCost() {
        item.setUpc("467321101899");
        item.setWeight(0.73);
		double expected = 6.57;
		double actual = air.getShippingCost(item);
		System.out.println(actual);
		assertEquals(expected,actual,.001);
		
	}
	
	@Rule
	public ExpectedException anException = ExpectedException.none();
	
	@Test
	public void testCost() {
		anException.expect(NullPointerException.class);
		//anException.expectMessage("Upc is null");
        item.setUpc(null);
        item.setWeight(0.73);
		double expected = 6.57;
		double actual = air.getShippingCost(item);
		System.out.println(actual);
		assertEquals(expected,actual,.001);
		
	}
	@After
	public void afterMethod() {
		System.out.println("Close the test===== After");
	}

}

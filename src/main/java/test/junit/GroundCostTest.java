package test.junit;

import static org.junit.Assert.*;

import javax.naming.NameNotFoundException;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.retail.core.Item;
import com.retail.display.DisplayGroundCost;

public class GroundCostTest {

	DisplayGroundCost ground;
	Item item;
	
	@Before
	public void doSetUp() {
		ground = new DisplayGroundCost();
		item = new Item();
		System.out.println("Opening of TestSuite====== Before");
	}
	@Test
	public void test1() {
		item.setWeight(3.21);
		Double expected = 8.025;
		Double actual=ground.getShippingCost(item);
		System.out.println(actual);
		assertEquals(expected,actual,.001);
	}
	
	@Test
	public void test2() {
		double a;
		item.setWeight(0.0);
		Double expected = 0.0;
		Double actual=ground.getShippingCost(item);
		System.out.println(actual);
		assertEquals(expected,actual,.001);
	}
	
	@Test
	public void test3() {
		item.setWeight(-6.58);
		Double expected = -16.45;
		Double actual=ground.getShippingCost(item);
		System.out.println(actual);
		assertEquals(expected,actual,.001);
	}
	@Rule
	public ExpectedException anException = ExpectedException.none();
	
	@Test
	public void testCost() {
		anException.expect(NullPointerException.class);
		//anException.expectMessage("null");
		item.setWeight(0.61);
		Double expected = 1.525;
		Double actual=ground.getShippingCost(null);
		System.out.println(actual);
		assertEquals(expected,actual,.001);
		
	}
	
	@After
	public void afterMethod() {
		System.out.println("Close the test===== After");
	}

}
